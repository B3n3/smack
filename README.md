SMACK Stack Scalability Framework
=================================

This repository contains tools to monitor applications based on SMACK and to automatically scale them.  
It was developed in the course of my master thesis, which can be found here:


Tutorial
-------

The following tutorial will enable you to launch the SMACK stack in a cluster hosted in AWS and deploy the 
applications implemented in this repository, as well as an IoT application, which is mainly I/O bound, and a 
data analytics application, which predicts elevator movements.

There are two repositories which are referred to:

1. https://github.com/Zuehlke/shmack
2. https://github.com/Zuehlke/hackzurich-sensordataanalysis/


### Environment Setup

Make sure you follow the instruction in the _SHMACK_ repository's README.  
It will guide you through the setup of:

* Virtual Machine (optional)
* AWS setup incl. AWS Console
* Intellij / Eclipse

### Launching the REST Monitoring Service

To launch an AWS EC2 instance which provides the REST monitoring service, where the extracted metrics are sent to, the 
script [create_stack.sh](src/smackRestMonitor/src/main/resources/create-stack.sh).  
After the cloud formation template is finished with the creation, you can find the URL of the service under _output_.

### Launching the DC/OS Cluster

Before you start, you can optionally configure various options in the `shmack_env` file.  
Launching the cluster on AWS with preinstalled [DC/OS](https://dcos.io/) is just as easy as executing the `create_stack.sh`
in the `scripts` folder.
After the cloud formation template creation is finished, the script will install the DC/OS command line for you, which
uses the `sudo` command and therefore you'll need to enter your password here.  
To connect your command line with the cluster, please follow the displayed link and authenticate yourself, eg. via Github
and paste the token into the command line.  
The script is now finished and opens the DC/OS cluster dashboard, which requires you again to log in.

You're now all set and can access the cluster via the dashboard as well as the CLI.

### Deploying the SMACK Technologies and the REST monitoring Service

To let the REST monitoring service know where to send the data, the ULR has to be passed to the Docker images via the 
json config, which is used by Marathon to deploy it in the cluster.
Take the URL gathered from the AWS Cloud Formation Dashboard of the REST monitoring service and put it into the 
[config file](src/jmxExtractor/jmxExtractor_marathon.json).

Now `init-smack-stack.sh` can be executed and the progress is visible in the DC/OS dashboard. 


### Deploying the Applications

In the `init-smack-applications.sh` script, command line snippets are collected which can be used to launch the desired applications.
It is important to *first* create the Kafka partition(s) and *then* deploy the Marathon apps or Spark jobs.  
In case you simply want to deploy everything, executing the script will achieve it.  
_Hint: As some applications might depend on each other, it is adviced to deploy one after each other to avoid problems._  
*Important:* Some paths are related to local config files (because we are using multiple repositories), which need
to be updated beforehand.  
Please make sure, that the _Spark DataAnalytics_ job must not be submitted before the Cassandra database is initialized by
the _KafkaTo{Cassandra,Accelerometer}_ job.


### Deploying & Starting the Load Generator

To facilitate large amounts of Docker images, AWS ECS (Elastic Container Service) is used.
The initial creation of the ECS cluster has to be done via the AWS dashboard under ECS.

1. Go to "Clusters" and click on "Create Cluster".
2. Choose Linux and hit "Next step".
3. You can choose a custom cluster name, or take the one which is used in the provided scripts: `t2-micro-15`
4. Choose `t2-micro` as EC2 instance type.
5. Choose 15 instances.
6. Scroll to the bottom and click on "Create".

Further, a "Task Definition" has to be created in the ECS console.
To take advantage of the scripts in this repository, name the task definition `smack-load-small`
and select `bwedenik/smack-load-generator-small` as Docker image.

Now the ECS cluster is launched.
In the mean time, the URL of our `sensor-ingestion` application running in the SMACK stack needs to be gathered.
Simply execute the `get-public-ip.sh` script to obtain an IP address.  
The ingestion service can be referred to via `http://<IP>:8083/sensorReading/<SensorName>`, where `SensorName` can be a string
which is used as key when writing into the Kafka topic.  
Put the URL into the [run-task.json](src/awsEcs/run-task.json) config file.
Now the ECS cluster can be populated by using the [run-tasks.sh](src/awsEcs/run-tasks.sh) script with the following usage:
`run-tasks.sh <number of Tasks (1-10)>`

To see a simple statistic about ingested sensor data, open the URL in the browser and use this as username and password: `hackzurich`

### Web Services

* `sensor-ingestion` - can be found under `http://<IP>:8083/sensorReading/`   
  There are no further pages.
* `akka-data-analytics` - can be found under `http://<IP>:8084/`
    * `/` - Overview
    * `/combined` - Combined data from batch + speed layer (if available)
    * `/speedLayer` - Shows speed layer data with prediction
    * `/speedLayerData` - Only shows data from speed layer
    * `/sparkData` - Only shows data from the batch layer a.k.a. the Spar job
* `REST Monitoring Service` - the URL can be found in the AWS Cloud Formation dashboard   
  There is a very basic webpage which allows you to investigate the collected metrics


### Stopping the cluster

To stop the cluster, the `delete-stack.sh` script can be executed.  
Another way is to use the AWS Cloud Formation template to delete the created stacks.