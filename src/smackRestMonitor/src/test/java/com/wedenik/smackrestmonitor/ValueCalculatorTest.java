package com.wedenik.smackrestmonitor;

import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class ValueCalculatorTest {

    private static final int N = 60;

    @Test
    public void getLatest() throws Exception {
        List<DateValueHostname> values = new ArrayList<>();
        values.add(new DateValueHostname("Mon Jul 31 10:00:00 UTC 2017", "1", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:10 UTC 2017", "100", "host 2"));
        ValueCalculator valueCalculator = new ValueCalculator(values, N);


        String result = valueCalculator.getLatest();
        assertEquals("100", result);
    }

    @Test
    public void getMaxNSec() throws Exception {
        List<DateValueHostname> values = new ArrayList<>();
        values.add(new DateValueHostname("Mon Jul 31 09:59:30 UTC 2017", "4", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 09:59:31 UTC 2017", "3", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:01 UTC 2017", "2", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:10 UTC 2017", "1", "host 2"));
        ValueCalculator valueCalculator = new ValueCalculator(values, N);
        ValueCalculator spyCalc = spy(valueCalculator);
        when(spyCalc.getNow()).thenReturn(ZonedDateTime.parse("Mon Jul 31 10:00:30 UTC 2017", DateValueHostname.getFormatter()));

        String result = spyCalc.getMaxNSec();
        assertEquals("3", result);
    }

    @Test
    public void getAvgNSec() throws Exception {
        List<DateValueHostname> values = new ArrayList<>();
        values.add(new DateValueHostname("Mon Jul 31 09:59:00 UTC 2017", "999999", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:00 UTC 2017", "000", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:01 UTC 2017", "20", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:10 UTC 2017", "50", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:20 UTC 2017", "100", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:25 UTC 2017", "200", "host 2"));
        ValueCalculator valueCalculator = new ValueCalculator(values, N);
        ValueCalculator spyCalc = spy(valueCalculator);
        when(spyCalc.getNow()).thenReturn(ZonedDateTime.parse("Mon Jul 31 10:00:30 UTC 2017", DateValueHostname.getFormatter()));

        String result = spyCalc.getAvgNSec();
        assertEquals("74.0", result);
    }

    @Test
    public void getMaxAvgNSecPerHost() throws Exception {
        List<DateValueHostname> values = new ArrayList<>();
        values.add(new DateValueHostname("Mon Jul 31 09:59:00 UTC 2017", "999999", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:00 UTC 2017", "000", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:01 UTC 2017", "20", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:10 UTC 2017", "0", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:20 UTC 2017", "100", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:25 UTC 2017", "200", "host 2"));
        ValueCalculator valueCalculator = new ValueCalculator(values, N);
        ValueCalculator spyCalc = spy(valueCalculator);
        when(spyCalc.getNow()).thenReturn(ZonedDateTime.parse("Mon Jul 31 10:00:30 UTC 2017", DateValueHostname.getFormatter()));

        String result = spyCalc.getMaxAvgNSecPerHost();
        assertEquals("100.0", result);
    }


    @Test
    public void getMedianNSec() throws Exception {
        List<DateValueHostname> values = new ArrayList<>();
        values.add(new DateValueHostname("Mon Jul 31 09:59:00 UTC 2017", "999999", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:00 UTC 2017", "000", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:01 UTC 2017", "20", "host 1"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:10 UTC 2017", "50", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:20 UTC 2017", "100", "host 2"));
        values.add(new DateValueHostname("Mon Jul 31 10:00:25 UTC 2017", "200", "host 2"));
        ValueCalculator valueCalculator = new ValueCalculator(values, N);
        ValueCalculator spyCalc = spy(valueCalculator);
        when(spyCalc.getNow()).thenReturn(ZonedDateTime.parse("Mon Jul 31 10:00:30 UTC 2017", DateValueHostname.getFormatter()));

        String result = spyCalc.getMedianNSec();
        assertEquals("50.0", result);
    }

}