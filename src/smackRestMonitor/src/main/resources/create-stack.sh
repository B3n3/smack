#!/bin/sh

STACK_NAME="EC2-smack-rest-monitor"
KEY_PAIR_NAME="ec2_general_purpose"
INSTANCE_TYPE="t1.micro"
TEMPLATE_PARAMETERS="ParameterKey=KeyName,ParameterValue=${KEY_PAIR_NAME} ParameterKey=InstanceType,ParameterValue=${INSTANCE_TYPE}"

aws cloudformation create-stack --stack-name ${STACK_NAME} --template-body file://ec2_cloudFormation_template.json --capabilities CAPABILITY_IAM --parameters ${TEMPLATE_PARAMETERS}

