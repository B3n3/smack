#!/usr/bin/python

import pygal
import sys
import dateutil.parser


outDir = ""
outHTMLFile = "index.html"
TIME_DIFF_THRESHOLD = 15

#check for arguments
if len(sys.argv) < 3:
    print "Usage: ", sys.argv[0], " <input.csv> <outputDirectory/> [htmlFilename]"
    sys.exit(1)

# prefix is filename without extension
filePrefix = sys.argv[1].split("/")[-1].split(".")[0].strip()
inFile = open(str(sys.argv[1]), "r")
data = {}
isFirstLine = 1

outDir = sys.argv[2]

if len(sys.argv) == 4:
    outHTMLFile = sys.argv[3]

#read all lines of the given file
for line in inFile:
    #skip csv header
    if(isFirstLine == 1):
        isFirstLine = 0
        continue

    tmp = line.split(";")
    _bean = tmp[0].strip()
    _attribute = tmp[1].strip()
    _date = tmp[2].strip()
    _hostname = tmp[3].strip()
    _dateHostnameValue = tmp[4].strip()

    #fill dict with data
    attributeMap = data.get(_bean, {})
    lst = attributeMap.get(_attribute, [])
    lst.append((_date, _hostname, _dateHostnameValue))
    attributeMap[_attribute] = lst
    data[_bean] = attributeMap


def createPlotObject():
    plot = pygal.DateTimeLine(x_label_rotation=270, truncate_label=-1, truncate_legend=-1, x_value_formatter=lambda dt: dt.strftime('%d.%m.%y %H:%M:%S'))
    plot.force_uri_protocol = 'http'   # used to show tooltips in browser
    return plot

svgCounter = 1
for bean in data.keys():
    chart_separate = createPlotObject()
    chart_avg = createPlotObject()
    chart_sum = createPlotObject()
    chart_separate.title = bean
    chart_avg.title = bean
    chart_sum.title = bean
    for attribute in data[bean].keys():
        yVals = {}
        yValsAvg = {}
        for dateHostnameValue in data[bean][attribute]:
            #parse date and convert value to float
            date = dateutil.parser.parse(dateHostnameValue[0])
            value = -1.0 #default for non-parsable input
            try:
                value = float(dateHostnameValue[2])
            except ValueError:
                print "Could not parse ", dateHostnameValue[2], "  setting it to ", str(value)
            #add values to list per hostname
            listPerHost = yVals.get(dateHostnameValue[1], [])
            listPerHost.append((date, value))
            yVals[dateHostnameValue[1]] = listPerHost

            #go and find entries within the same timestamp
            valueForDate = 0.0
            dateToUpdate = date
            valueCounter = 0
            for date_ in yValsAvg.keys():
                timediff = abs((date - date_).total_seconds())
                if(timediff < TIME_DIFF_THRESHOLD):
                    valueForDate = yValsAvg[date_][0]
                    valueCounter = yValsAvg[date_][1]
                    dateToUpdate = date_

            valueForDate = valueForDate + value
            valueCounter = valueCounter + 1
            yValsAvg[dateToUpdate] = (valueForDate, valueCounter)


        # calculate the average by dividing the value by the count
        averagedValues =  {k: v[0] / v[1] for (k, v) in yValsAvg.items()}
        sumValues =  {k: v[0] for (k, v) in yValsAvg.items()}

        # sort dict by date before adding it to the plot
        chart_avg.add(attribute + " (averaged)", sorted(averagedValues.items(), key=lambda x: x[0]))
        chart_sum.add(attribute + " (summed up)", sorted(sumValues.items(), key=lambda x: x[0]))

        for hostname in yVals.keys():
            chart_separate.add(attribute + " " + hostname, yVals[hostname])

    chart_separate.render_to_file(outDir + filePrefix + str(svgCounter) + "_separate.svg")
    chart_avg.render_to_file(outDir + filePrefix + str(svgCounter) + "_avg.svg")
    chart_sum.render_to_file(outDir + filePrefix + str(svgCounter) + "_sum.svg")
    svgCounter = svgCounter + 1


#generate HTML file containing a list of the charts
html = open(outDir + outHTMLFile, "a")
for i in range(1,svgCounter):
    html.write("<a href=\"" + filePrefix + str(i) + "_separate.svg\">" + filePrefix + str(i) + "_separate.svg</a><br />\n")
    html.write("<a href=\"" + filePrefix + str(i) + "_avg.svg\">" + filePrefix + str(i) + "_avg.svg</a><br />\n")
    html.write("<a href=\"" + filePrefix + str(i) + "_sum.svg\">" + filePrefix + str(i) + "_sum.svg</a><br />\n")
html.close()

