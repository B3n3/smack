package com.wedenik.smackrestmonitor;

import java.util.logging.Logger;

class Cassandra extends RESTHandler {
    Cassandra(String outFile) {
        super(outFile);
    }

    @Override
    String overview() {
        return "<h1>Cassandra</h1>" +
                "<a href =\"cassandra/dump\">cassandra/dump</a> <br />" +
                "<a href =\"cassandra/export\">cassandra/export</a> <br />" +
                "<a href =\"cassandra/availableKeys\">cassandra/availableKeys</a> <br />" +
                "cassandra/availableAttributes/:bean <br />" +
                "cassandra/get/:bean/:attribute <br />";
    }

    @Override
    void updateLogger() {
        this.log = Logger.getLogger(Cassandra.class.toString());
    }

    @Override
    String getLowercaseName() {
        return "cassandra";
    }
}
