package com.wedenik.smackrestmonitor;

class AttributeNotFoundException extends NotFoundException {
    AttributeNotFoundException() {
        super("Attribute not found");
    }
}
