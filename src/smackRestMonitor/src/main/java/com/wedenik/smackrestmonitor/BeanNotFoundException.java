package com.wedenik.smackrestmonitor;

class BeanNotFoundException extends NotFoundException {
    BeanNotFoundException() {
        super("Bean not found");
    }
}
