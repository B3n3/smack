package com.wedenik.smackrestmonitor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static spark.Spark.*;

public class SmackRestMonitor {
    private final static Logger log = Logger.getLogger(SmackRestMonitor.class.toString());
    private final static RESTHandler kafka = new Kafka("/var/www/html/kafka.csv");
    private final static RESTHandler cassandra = new Cassandra("/var/www/html/cassandra.csv");
    private final static RESTHandler akka = new Akka("/var/www/html/akka.csv");
    private final static RESTHandler spark = new Spark("/var/www/html/spark.csv");
    private final static RESTHandler sparkDataAnalytics = new SparkDataAnalytics("/var/www/html/spark-data-analytics.csv");
    private static int PORT = 9876;
    private static final String SVG_DIR = "/var/www/html/svgs/";
    private static final String SVG_HTML_FILENAME = "index.html";
    private static final String PLOTTER_SCRIPT = "/tmp/plotter.py";
    private static final String LIGHTTPD_PORT = "8080";


    private static boolean firstRun = true;
    private final static Set<RESTHandler> handlers = new HashSet<>();

    public static void main(String[] args) {
        if (args.length == 1) {
            try {
                PORT = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                log.warning("Could not parse PORT! " + args[0]);
            }
        }

        port(PORT);
        staticFiles.externalLocation(SVG_DIR);
        init();

        log.info("Starting up at PORT " + PORT);

        get("/", (req, res) -> {
                    return "<a href=\"/akka\">akka</a> <br />" +
                            "<a href=\"/cassandra\">cassandra</a> <br />" +
                            "<a href=\"/spark\">spark</a> <br />" +
                            "<a href=\"/spark-data-analytics\">spark-data-analytics</a> <br />" +
                            "<a href=\"/kafka\">kafka</a> <br />" +
                            "<br />" +
                            "<a href=\"/generatePlots\">generatePlots (SLOW!)</a> <br />" +
                            "<a href=\"/svgs/\" onclick=\"javascript:event.target.port=" + LIGHTTPD_PORT + "\">SVGs</a> <br />" +
                            "<a href=\"/all.zip\" onclick=\"javascript:event.target.port=" + LIGHTTPD_PORT + "\">all.zip</a>";
                }
        );

        handlers.add(akka);
        handlers.add(cassandra);
        handlers.add(kafka);
        handlers.add(spark);
        handlers.add(sparkDataAnalytics);

        for (RESTHandler h : handlers) {
            installHandler(h);
        }
        get("/generatePlots", (req, res) ->
                createPlots()
        );

    }


    private static void installHandler(RESTHandler restHandler) {
        String serviceName = restHandler.getLowercaseName();
        get("/" + serviceName, (req, res) -> restHandler.overview());
        get("/" + serviceName + "/dump", (req, res) -> restHandler.dumpData("<br />"));
        get("/" + serviceName + "/export", (req, res) -> {
            restHandler.exportCSV();
            res.type("text/comma-separated-values");
            return restHandler.dumpData("");
        });
        post("/" + serviceName + "/add", (req, res) -> {
            restHandler.handleRequest(
                    req.queryParams("date"),
                    req.queryParams("bean"),
                    req.queryParams("attribute"),
                    req.queryParams("value"),
                    req.queryParams("hostname")
            );
            return "Thanks!";
        });
        get("/" + serviceName + "/availableKeys", (req, res) -> restHandler.availableKeys());
        get("/" + serviceName + "/availableAttributes/:bean", (req, res) -> restHandler.getBeanAttributes(req.params(":bean")));
        get("/" + serviceName + "/get/:bean/:attribute", (req, res) -> {
            try {
                return restHandler.getAllData(req.params(":bean"), req.params(":attribute"));
            } catch (NotFoundException e) {
                res.status(404);
                return e.getMessage();
            }
        });
        get("/" + serviceName + "/get/:bean/:attribute/:value", (req, res) -> {
            try {
                return restHandler.getData(req.params(":bean"), req.params(":attribute"), req.params(":value"));
            } catch (NotFoundException e) {
                res.status(404);
                return e.getMessage();
            }
        });
    }


    private static String createPlots() {
        for (RESTHandler handler : handlers) {
            handler.exportCSV();
        }
        try {
            Process p = Runtime.getRuntime().exec("rm -r " + SVG_DIR);
            p.waitFor();
            p = Runtime.getRuntime().exec("mkdir " + SVG_DIR);
            p.waitFor();
            if (firstRun) {
                p = Runtime.getRuntime().exec("service lighttpd start");
                p.waitFor();
                firstRun = false;
            }
        } catch (IOException | InterruptedException e) {
            String error = "Tried to clean up and create" + SVG_DIR + " but failed: " + e.getMessage();
            log.warning(error);
            return error;
        }

        for (RESTHandler handler : handlers) {
            String cmd = "python " + PLOTTER_SCRIPT + " " + handler.getOutFile() + " " + SVG_DIR + " " + SVG_HTML_FILENAME;
            log.info("Running this command: '" + cmd + " '");
            try {
                String s;
                Process p = Runtime.getRuntime().exec(cmd);
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(p.getErrorStream()));
                while ((s = br.readLine()) != null)
                    System.out.println("line: " + s);
                br = new BufferedReader(
                        new InputStreamReader(p.getInputStream()));
                while ((s = br.readLine()) != null)
                    System.out.println("line: " + s);
                p.waitFor();
                System.out.println("exit: " + p.exitValue());
                p.destroy();

            } catch (IOException | InterruptedException e) {
                String error = "Tried to execute this command: '" + cmd + " ' but got this error: " + e.getMessage();
                log.warning(error);
                return error;
            }
        }

        createZip();

        return "Successfully generated <a href=\"/svgs/\" onclick=\"javascript:event.target.port=" + LIGHTTPD_PORT + "\">SVGs</a>";
    }

    private static void createZip() {
        try {
            Process p = Runtime.getRuntime().exec("zip -r /var/www/html/all.zip /var/www/html/");
            p.waitFor();
            log.info("Successfully created /var/www/html/all.zip");
        } catch (IOException | InterruptedException e) {
            String error = "Tried to create zip but failed: " + e.getMessage();
            log.warning(error);
        }
    }

}
