package com.wedenik.smackrestmonitor;

import java.util.logging.Logger;

class Akka extends RESTHandler {
    Akka(String outfile) {
        super(outfile);
    }


    @Override
    String overview() {
        return "<h1>Akka</h1>" +
                "<a href =\"akka/dump\">akka/dump</a> <br />" +
                "<a href =\"akka/export\">akka/export</a> <br />" +
                "<a href =\"akka/availableKeys\">akka/availableKeys</a> <br />" +
                "akka/availableAttributes/:bean <br />" +
                "akka/get/:bean/:attribute <br />";
    }

    @Override
    void updateLogger() {
        this.log = Logger.getLogger(Akka.class.toString());

    }

    @Override
    String getLowercaseName() {
        return "akka";
    }
}
