package com.wedenik.smackrestmonitor;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Container class for entries in the CSV
 */
public class DateValueHostname implements Comparable<DateValueHostname> {
    final String date;
    final ZonedDateTime date_parsed;
    final String value;
    final double value_parsed;
    final String hostname;
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss zzz yyyy");

    DateValueHostname(String date, String value, String hostname) {
        this.date = date;
        this.value = value;
        this.hostname = hostname;
        ZonedDateTime parsedDate = null;
        double parsedValue = -1.0;
        try {
            parsedDate = ZonedDateTime.parse(date.replaceAll("  ([0-9]) ", " 0$1 "), formatter);
        } catch (DateTimeParseException e) {
            System.out.println("Could not parse this date: " + e.getParsedString() + "  " + e.getMessage());
        }
        date_parsed = parsedDate;
        try {
            parsedValue = Double.valueOf(value);
        } catch (NumberFormatException e) {
            System.out.println("Could not parse this value: " + value + "  " + e.getMessage());
        }
        value_parsed = parsedValue;
    }

    public static DateTimeFormatter getFormatter() {
        return formatter;
    }

    @Override
    public String toString() {
        return date + " ; " + hostname + " ; " + value;
    }

    @Override
    public int compareTo(DateValueHostname o) {
        if (this.value_parsed < o.value_parsed)
            return -1;
        else if (o.value_parsed < this.value_parsed)
            return 1;
        return 0;
    }
}
