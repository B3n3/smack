package com.wedenik.smackrestmonitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * Container class for handling request and storing POST data
 */
abstract class RESTHandler {
    Logger log;

    private String outFile;
    private Map<String, Map<String, List<DateValueHostname>>> container = new HashMap<>();

    RESTHandler(String outFile) {
        this.outFile = outFile;
        updateLogger();
    }

    public String getOutFile() {
        return outFile;
    }

    void handleRequest(String date, String bean, String attribute, String value, String hostname) {
        updateContainer(bean, attribute, date, value, hostname);
        //logRequest(date, bean, attribute, value, hostname);
    }

    private void updateContainer(String bean, String attribute, String date, String value, String IP) {
        Map<String, List<DateValueHostname>> attributeMap = container.getOrDefault(bean, new HashMap<>());
        List<DateValueHostname> lst = attributeMap.getOrDefault(attribute, new ArrayList<>());
        lst.add(new DateValueHostname(date, value, IP));
        attributeMap.put(attribute, lst);
        container.put(bean, attributeMap);
    }

    private String containerToString(String br) {
        StringBuilder sb = new StringBuilder();
        for (String bean : container.keySet()) {
            for (String attribute : container.get(bean).keySet()) {
                for (DateValueHostname dateValueIp : container.get(bean).get(attribute)) {
                    sb.append(bean);
                    sb.append(" ; ");
                    sb.append(attribute);
                    sb.append(" ; ");
                    sb.append(dateValueIp.date);
                    sb.append(" ; ");
                    sb.append(dateValueIp.hostname);
                    sb.append(" ; ");
                    sb.append(dateValueIp.value);
                    sb.append(br);
                    sb.append("\n");
                }
            }
        }
        return sb.toString();
    }

    String dumpData(String br) {
        return ("Bean ; Attribute ; Date ; Hostname ; Value" + br + "\n") +
                containerToString(br);
    }

    void exportCSV() {
        try {
            PrintWriter writer = new PrintWriter(outFile, "UTF-8");
            writer.print(dumpData(""));
            writer.close();
        } catch (IOException e) {
            System.err.println("Failed to write into file!");
        }
    }

    private void logRequest(String date, String bean, String attribute, String value, String hostname) {
        String sb = "Got a request from " + hostname +
                "\n Date " + date +
                " ; Bean " + bean +
                " ; Attribute " + attribute +
                " ; Value " + value;
        log.info(sb);
    }

    String availableKeys() {
        StringBuilder sb = new StringBuilder();
        for (String bean : container.keySet()) {
            sb.append(bean);
            sb.append("<br />\n");
        }
        return sb.toString();
    }

    String getBeanAttributes(String bean) {
        StringBuilder sb = new StringBuilder();
        for (String att : container.get(bean).keySet()) {
            sb.append(att);
            sb.append("<br />\n");
        }
        return sb.toString();
    }

    String getAllData(String bean, String attribute) throws NotFoundException {
        if (container.get(bean) == null) {
            throw new BeanNotFoundException();
        }
        if (container.get(bean).get(attribute) == null) {
            throw new AttributeNotFoundException();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Date ; Hostname ; Value <br/ > \n");
        for (DateValueHostname v : container.get(bean).get(attribute)) {
            sb.append(v);
            sb.append("<br /> \n");
        }
        return sb.toString();
    }

    String getData(String bean, String attribute, String value) throws NotFoundException {
        if (container.get(bean) == null) {
            throw new BeanNotFoundException();
        }
        final List<DateValueHostname> values = container.get(bean).get(attribute);
        if (values == null) {
            throw new AttributeNotFoundException();
        }

        ValueCalculator calculator = new ValueCalculator(values, 60);

        switch (value) {
            case "LATEST":
                return calculator.getLatest();
            case "MAX_60_SEC":
                return calculator.getMaxNSec();
            case "AVG_60_SEC":
                return calculator.getAvgNSec();
            case "MAX_AVG_60_SEC_PER_HOST":
                return calculator.getMaxAvgNSecPerHost();
            case "MEDIAN_60_SEC":
                return calculator.getMedianNSec();
            default:
                return "Invalid type of value";
        }
    }

    abstract String overview();

    abstract void updateLogger();

    abstract String getLowercaseName();

}
