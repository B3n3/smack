package com.wedenik.smackrestmonitor;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility for calculating values to return in REST
 */
class ValueCalculator {
    private final List<DateValueHostname> values;
    private List<DateValueHostname> lastNSeconds = new ArrayList<>();
    private final String noDataReturnMessage;
    private final int N;


    ValueCalculator(List<DateValueHostname> values, int N) {
        this.values = values;
        this.noDataReturnMessage = "There was no data in the last " + N + " seconds";
        this.N = N;
    }

    private void updateLastNSecList() {
        lastNSeconds = values.stream().filter(x -> ChronoUnit.SECONDS.between(x.date_parsed, getNow()) < N).collect(Collectors.toList());
    }


    String getLatest() {
        return values.get(values.size() - 1).value;
    }

    String getMaxNSec() {
        updateLastNSecList();
        if (lastNSeconds.isEmpty()) {
            return noDataReturnMessage;
        }
        return Collections.max(lastNSeconds).value;
    }

    String getAvgNSec() {
        updateLastNSecList();
        if (lastNSeconds.isEmpty()) {
            return noDataReturnMessage;
        }
        return String.valueOf(lastNSeconds.stream().
                mapToDouble(x -> x.value_parsed).
                summaryStatistics().
                getAverage());
    }

    String getMaxAvgNSecPerHost() {
        updateLastNSecList();
        if (lastNSeconds.isEmpty()) {
            return noDataReturnMessage;
        }
        List<Double> avgPerHost = new ArrayList<>();
        for (String host : lastNSeconds.stream().map(x -> x.hostname).distinct().collect(Collectors.toList())) {
            avgPerHost.add(lastNSeconds.stream().
                    filter(x -> x.hostname.equals(host)).
                    mapToDouble(x -> x.value_parsed).
                    summaryStatistics().
                    getAverage());
        }
        return String.valueOf(Collections.max(avgPerHost));
    }

    String getMedianNSec() {
        updateLastNSecList();
        if (lastNSeconds.size() < 2) {
            return noDataReturnMessage;
        }
        Collections.sort(lastNSeconds);
        double median;
        if (lastNSeconds.size() % 2 == 0)
            median = (lastNSeconds.get(lastNSeconds.size() / 2).value_parsed + lastNSeconds.get(lastNSeconds.size() / 2 - 1).value_parsed) / 2;
        else
            median = lastNSeconds.get(lastNSeconds.size() / 2).value_parsed;
        return String.valueOf(median);
    }

    /**
     * Exists to be able to mock it
     */
    ZonedDateTime getNow() {
        return ZonedDateTime.now();
    }

}
