package com.wedenik.smackrestmonitor;

abstract class NotFoundException extends Throwable {
    NotFoundException(String message) {
        super(message);
    }
}
