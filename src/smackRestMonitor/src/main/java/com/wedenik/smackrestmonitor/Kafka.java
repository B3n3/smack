package com.wedenik.smackrestmonitor;

import java.util.logging.Logger;

class Kafka extends RESTHandler {
    Kafka(String outFile) {
        super(outFile);
    }


    @Override
    String overview() {
        return "<h1>Kafka</h1>" +
                "<a href =\"kafka/dump\">kafka/dump</a> <br />" +
                "<a href =\"kafka/export\">kafka/export</a> <br />" +
                "<a href =\"kafka/availableKeys\">kafka/availableKeys</a> <br />" +
                "kafka/availableAttributes/:bean <br />" +
                "kafka/get/:bean/:attribute <br />";
    }

    @Override
    void updateLogger() {
        this.log = Logger.getLogger(Kafka.class.toString());

    }

    @Override
    String getLowercaseName() {
        return "kafka";
    }
}
