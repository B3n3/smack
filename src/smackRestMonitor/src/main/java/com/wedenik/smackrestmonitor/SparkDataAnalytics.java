package com.wedenik.smackrestmonitor;

import java.util.logging.Logger;

class SparkDataAnalytics extends RESTHandler {
    SparkDataAnalytics(String outFile) {
        super(outFile);
    }


    @Override
    String overview() {
        return "<h1>Spark</h1>" +
                "<a href =\"spark-data-analytics/dump\">spark/dump</a> <br />" +
                "<a href =\"spark-data-analytics/export\">spark/export</a> <br />" +
                "<a href =\"spark-data-analytics/availableKeys\">spark/availableKeys</a> <br />" +
                "spark-data-analytics/availableAttributes/:bean <br />" +
                "spark-data-analytics/get/:bean/:attribute <br />";
    }

    @Override
    void updateLogger() {
        this.log = Logger.getLogger(SparkDataAnalytics.class.toString());

    }

    @Override
    String getLowercaseName() {
        return "spark-data-analytics";
    }
}
