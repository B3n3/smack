package com.wedenik.smackrestmonitor;

import java.util.logging.Logger;

class Spark extends RESTHandler {
    Spark(String outFile) {
        super(outFile);
    }


    @Override
    String overview() {
        return "<h1>Spark</h1>" +
                "<a href =\"spark/dump\">spark/dump</a> <br />" +
                "<a href =\"spark/export\">spark/export</a> <br />" +
                "<a href =\"spark/availableKeys\">spark/availableKeys</a> <br />" +
                "spark/availableAttributes/:bean <br />" +
                "spark/get/:bean/:attribute <br />";
    }

    @Override
    void updateLogger() {
        this.log = Logger.getLogger(Spark.class.toString());

    }

    @Override
    String getLowercaseName() {
        return "spark";
    }
}
