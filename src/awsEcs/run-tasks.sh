#!/bin/sh

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    echo "Usage: `basename $0` <number of Tasks (1-10)>"
    exit 1
fi

aws ecs run-task --cluster t2-micro-15 --task-definition smack-load-small --count $1 --overrides file://run-task.json
