#!/usr/bin/perl

use LWP::UserAgent;
use HTTP::Request::Common qw{ GET };
use URI::Escape;

# apt-get install liblog-log4perl-perl
use Log::Log4perl qw(:easy);
Log::Log4perl->easy_init($DEBUG);

# get host from commandline
if(defined $ARGV[0]) {
    $HOST = $ARGV[0];
}
else {
    print "Usage: $0 <hostURL> \n";
    exit(0);
}


$UA = LWP::UserAgent->new;
$HOST;
$SLEEP_TIMEOUT = 5;
$DEPLOYMENT_TIMEOUT = 180;
$KAFKA_DEPLOYMENT_TIMEOUT = 0;

%kafkaHashes = (
    makeUrl("kafka", "kafka.network:type=RequestMetrics,name=TotalTimeMs,request=FetchFollower", "Mean", "MAX_60_SEC") => 400,
    #makeUrl("kafka", "kafka.network:type=RequestMetrics,name=TotalTimeMs,request=FetchConsumer", "Mean", "MAX_60_SEC") => 400, ### does not recover fast enough -> i.e. the value stays high
    makeUrl("kafka", "kafka.server:type=ReplicaManager,name=UnderReplicatedPartitions", "Value", "MAX_60_SEC") => 1,
    makeUrl("kafka", "kafka.controller:type=KafkaController,name=OfflinePartitionsCount", "Value", "MAX_60_SEC") => 1
);

%akkaHashes = (
    makeUrl("akka", "kamon:name=my-system/user/ingestion-actor.processing-time,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST") => 5e+9,
    makeUrl("akka", "kamon:name=my-system/user/ingestion-actor.time-in-mailbox,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST") => 2e+13,
    makeUrl("akka", "kamon:name=my-system/user/ingestion-actor.mailbox-size,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST") => 200000,
);

%allHashes = (%kafkaHashes, %akkaHashes);



INFO "Starting to monitor ", scalar keys %allHashes, " values\n";

while(1) {
    my $nothingTodo = 1;
    for my $url(keys %allHashes) {
        my $req = GET($url);
        my $resp = $UA->request($req);
        if ($resp->is_success) {
            my $value = $resp->decoded_content;
            my $threshold = $allHashes{$url};
            my $bean = $url;
            my $service = $url;

            # parse service name and bean from url
            if($url =~/\/([a-z]+?)\/get\/(.*)/) {
                $service = $1;
                $bean = $2;
            }

            if($value >= $threshold) {
                INFO "You should consider scaling up ", $service, "\n", "The value of ", $bean, " exceeded the threshold of ", $threshold, " and is now **", $value, "**\n\n";
                if($service eq "kafka") {
                    scaleUpKafka();
                }
                if($service eq "cassandra") {
                    scaleUpCassandra();
                }
                $nothingTodo = 0;
            } else {
                TRACE "Nothing to do for ", $service, " : ", $bean, " as value is: ", $value, " Maybe implement scaling down.\n";
            }

        } else {
            ERROR "HTTP error: ", $resp->code, " ", $resp->message, ", ", $resp->decoded_content, "  // when trying ", $url, "\n";
        }
    }

    if($nothingTodo == 1) {
        INFO "Everything is fine. Nothing to do.\n\n";
    }
    else {
        INFO "YOU SHOULD DO SOMETHING!\n";
    }

    print "\n----------------------------------------------\n\n";
    sleep($SLEEP_TIMEOUT);
    decreaseDeploymentTimers();
}


sub scaleUpMarathonEnv {
    my ( $service, $fieldToUpdate ) = @_;
    my $cmdShowConfig = `dcos marathon app show $service`;

    # how many brokers / nodes to add?
    my $INCREMENT = 0; #TODO set this to 1

    my $firstLine = 1;
    my $inEnv = 0;
    my @output;

    # go through all the lines of the received JSON
    # keep the first line
    # keep all lines which are in the "env" list
    for my $line(split(/\n/, $cmdShowConfig)) {
        if($firstLine == 1) {
            $firstLine = 0;
            push @output, $line;
        } else {
            # check if we are in env now
            if($line =~ /"env"/) {
                $inEnv = 1;
            }

            # only keep lines if we are in env
            if($inEnv == 1) {
                # check if we are at the last line of env
                if($line =~ /},/) {
                    $inEnv = 0;
                    # replace the "," as we only have one element in the JSON object
                    $line =~ s/,//g;
                }
                push @output, $line;
            }
        }
    }
    # add the closing curly braces
    push @output, "}\n";

    #create a single string and increase the broker / node count by INCREMENT
    my $json = join("\n", @output);
    my $brokerCount = $json =~ /$fieldToUpdate.*([0-9]+)/ ;
    $brokerCount = $1 + $INCREMENT;
    my $jsonUpdated = $json =~ s/($fieldToUpdate.*)([0-9]+)/$1$brokerCount/r ;

    #write the new config into a file to pass it the dcos marathon command
    open(my $outFile, '>', "newConfig.$service.json");
    print $outFile $jsonUpdated;
    close($outFile);

    my $res = `dcos marathon app update $service < newConfig.$service.json`;
    print $res;
}

sub scaleUpKafka {
    if($KAFKA_DEPLOYMENT_TIMEOUT == 0) {
        INFO "I'll go and add another KAFKA Broker for you!";
        scaleUpMarathonEnv("kafka", "BROKER_COUNT");
        $KAFKA_DEPLOYMENT_TIMEOUT = $DEPLOYMENT_TIMEOUT;
    } elsif($KAFKA_DEPLOYMENT_TIMEOUT > 0) {
        DEBUG "There is a KAFKA deployment running, waiting ", $KAFKA_DEPLOYMENT_TIMEOUT, " seconds for next scaleUp.";
    }
}

sub scaleUpCassandra {
    if($CASSANDRA_DEPLOYMENT_TIMEOUT == 0) {
        INFO "I'll go and add another CASSANDRA node for you!";
        scaleUpMarathonEnv("cassandra", "NODES");
        $CASSANDRA_DEPLOYMENT_TIMEOUT = $DEPLOYMENT_TIMEOUT;
    } elsif($CASSANDRA_DEPLOYMENT_TIMEOUT > 0) {
        DEBUG "There is a CASSANDRA deployment running, waiting ", $CASSANDRA_DEPLOYMENT_TIMEOUT, " seconds for next scaleUp.";
    }
}

sub decreaseDeploymentTimers {
    if($KAFKA_DEPLOYMENT_TIMEOUT > 0) {
        $KAFKA_DEPLOYMENT_TIMEOUT -= $SLEEP_TIMEOUT;
        if($KAFKA_DEPLOYMENT_TIMEOUT < 0) {
            $KAFKA_DEPLOYMENT_TIMEOUT = 0;
        }
    }
    if($CASSANDRA_DEPLOYMENT_TIMEOUT > 0) {
        $CASSANDRA_DEPLOYMENT_TIMEOUT -= $SLEEP_TIMEOUT;
        if($CASSANDRA_DEPLOYMENT_TIMEOUT < 0) {
            $CASSANDRA_DEPLOYMENT_TIMEOUT = 0;
        }
    }
}

sub makeUrl {
    my ($service, $bean, $attribute, $value) = @_;
    return $HOST . "/" . $service . "/get/" . uri_escape($bean) . "/" . $attribute . "/" . $value;
}

