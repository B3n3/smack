#!/bin/sh

./generate_spark_jmxcommands.pl
./generate_spark-data-analytics_jmxcommands.pl

./collect_results.pl commands_cassandra "$1/cassandra/add" &
P1=$!
./collect_results.pl commands_kafka "$1/kafka/add" &
P2=$!
./collect_results.pl commands_akka "$1/akka/add" &
P3=$!
./collect_results.pl commands_spark "$1/spark/add" &
P4=$!
./collect_results.pl commands_spark-data-analytics "$1/spark-data-analytics/add" &
P5=$!

wait $P1 $P2 $P3 $P4 $P5

