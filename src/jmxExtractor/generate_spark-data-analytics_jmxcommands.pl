#!/usr/bin/perl

$DATE=`date`;
$DATE =~ s/\R//;
$JMXTREME_JAR = "jmxterm-1.0-alpha-4-uber.jar";
$JMXTREME = "java -jar $JMXTREME_JAR -n -v silent";
$JVM_ID = "localhost:8093";
$CMDS = "
open $JVM_ID
beans
close
";

#get all beans
@beans = split(/\n/, `echo "$CMDS" | $JMXTREME `);
@takenBeans = ();

$out = "open $JVM_ID\n";
%res;
$cmd = "open $JVM_ID\n";
for my $bean (@beans) {
    #only consider interesting beans
    if($bean =~ /DataAnalytics|-driver-/) {
        $cmd = $cmd . "info -b $bean\n";
        push @takenBeans, $bean;
    }
}
$cmd = $cmd . "close\n";

$attr = `echo "$cmd" | $JMXTREME`;
$beanCounter = -1;

for my $line (split(/\n/, $attr)) {
    if($line =~ /# attributes/) {
        $beanCounter = $beanCounter + 1;
    }
    if($line =~ /- ([A-Z][a-zA-Z]+) \(/g ) {
        $out = $out . "get -b $takenBeans[$beanCounter] $1\n";
    }
}

$out = $out . "close\n";

open(my $outFile, '>', 'commands_spark-data-analytics');
print $outFile $out;
close($outFile);
