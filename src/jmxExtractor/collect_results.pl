#!/usr/bin/perl

### use Data::Dumper qw(Dumper);
use LWP::UserAgent;
use HTTP::Request::Common qw{ POST };

$JMXTERM_JAR = "jmxterm-1.0-alpha-4-uber.jar";
$JMXTERM = "java -jar $JMXTERM_JAR -n -v silent";
$COMMAND_FILE;
$OUT_FILE;
$UA = LWP::UserAgent->new;
$HOSTNAME=`hostname`;
$HOSTNAME =~ s/\R//;
$SERVER_ENDPOINT;
$SLEEP_TIMEOUT = 15;
$DEBUG = 0;
$RERUN_SPARK_COMMAND_GENERATION_WAITS = 10;
$rerunSparkCommandGeneration = 0;


# Check commandline arguments
if(defined $ARGV[0] && defined $ARGV[1]) {
    $COMMAND_FILE = $ARGV[0];
    $OUT_FILE = "data_$COMMAND_FILE.csv";
    $SERVER_ENDPOINT = $ARGV[1];
}
else {
    print "Usage: $0 <commandFile> <MonitorURL> [sleepTimeout s]\n";
    exit(0);
}

# Check for optional timeout argument
if(defined $ARGV[2]) {
    $SLEEP_TIMEOUT = $ARGV[2];
}

while(1) {

    # Current timestamp without any whitespaces or newlines
    $DATE=`date`;
    $DATE =~ s/\R//;


    # Read provided JMX commands from file
    open($cmds, '<', $COMMAND_FILE) or die "Could not open file '$COMMAND_FILE' !";
    chomp(my @tmp_beans = <$cmds>);


    # Create a list of all beans in the given command file
    # We need those bean names as the output of the command does not contain the name
    for my $bean (@tmp_beans) {
        if($bean =~ /get -b (.+) [A-Z]/g) {
            push(@beans, $1)
        }
    }

    close($COMMAND_FILE);

    # Open CSV output file and add header if the file is empty
    open($csv_out, '>>', $OUT_FILE) or die "Could not open file '$OUT_FILE !";
    if(-z $OUT_FILE) {
        print $csv_out "Bean ; Attribute ; Date ; Hostname ; Value\n";
    }


    # Execute the jmxterm jar with the provided command file and split the lines
    @res = split(/\n/, `$JMXTERM < $COMMAND_FILE`);
    my $i = 0;
    for my $line(@res) {

        # The output contains the attribute name and the value
        if($line =~ /([A-Z]+[a-zA-Z]+) = (.*);/g) {
            print $csv_out "$beans[$i] ; $1 ; $DATE ; $HOSTNAME ; $2\n";

            # Create POST data for the HTTP request
            my $post_data = {"date" =>$DATE, "bean" => $beans[$i], "attribute" => $1, "value" => $2, "hostname" => $HOSTNAME};
            my $req = POST($SERVER_ENDPOINT, $post_data);
            my $resp = $UA->request($req);
            if($DEBUG) {
                if ($resp->is_success) {
                    my $message = $resp->decoded_content;
                    print "Received reply: $message\n";
                }
                else {
                    print "HTTP POST error code: ", $resp->code, "\n";
                    print "HTTP POST error message: ", $resp->message, "\n";
                }
            }
            $i++;
        }
    }

    print("Written $i lines\n");
    close $csv_out;

    # In case we have to monitor Spark, the bean names change from run to run,
    # because of unique IDs in the name.
    # Therefore we need to rerun the script to get a new updated input command file.
    if($COMMAND_FILE =~ /spark/) {
        if($rerunSparkCommandGeneration == $RERUN_SPARK_COMMAND_GENERATION_WAITS) {
            `./generate_spark_jmxcommands.pl` ;
            `./generate_spark-data-analytics_jmxcommands.pl` ;
            $rerunSparkCommandGeneration = 0;
        }
        else {
            $rerunSparkCommandGeneration += 1;
        }
    }

    sleep($SLEEP_TIMEOUT);
}

