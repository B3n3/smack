package com.wedenik.smackcontroller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.logging.Logger;

class Akka extends Service {

    private final String SERVICE_NAME;

    Akka(String hostUrl, String serviceName) {
        super(hostUrl);

        log = Logger.getLogger(Akka.class.toString());
        try {
            initHashes();
        } catch (UnsupportedEncodingException e) {
            log.warning("Error during initialization: " + e.getMessage());
        }
        SERVICE_NAME = serviceName;
    }

    private void initHashes() throws UnsupportedEncodingException {
        thresholds = new HashMap<>();
        thresholds.put(Util.createServiceWithUrl("akka", "kamon:name=my-system/user/ingestion-actor.processing-time,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST", hostUrl), (long) 2e+9);
        thresholds.put(Util.createServiceWithUrl("akka", "kamon:name=my-system/user/ingestion-actor.time-in-mailbox,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST", hostUrl), (long) 2e+13);
        thresholds.put(Util.createServiceWithUrl("akka", "kamon:name=my-system/user/ingestion-actor.mailbox-size,type=akka-actor", "Sum", "MAX_AVG_60_SEC_PER_HOST", hostUrl), 900L);
    }

    private void scale(int scalingIncrement) {
        if (deploymentTimeout == 0) {
            log.info("I'll go and add " + scalingIncrement + " AKKA instance(s) for you!");

            String jsonConfig = getMarathonAppConfig(SERVICE_NAME);
            if (jsonConfig == null) {
                log.warning("Could not get config for " + SERVICE_NAME);
                return;
            }
            int updatedInstanceAmount = getUpdatedFieldValue(SERVICE_NAME, "instances", jsonConfig, scalingIncrement);
            if (updatedInstanceAmount == -1) {
                log.warning("Could not extract value of instances to update.");
                return;
            }
            log.info("New number of " + SERVICE_NAME + " instances is " + updatedInstanceAmount);

            final String cmd = "dcos marathon app update " + SERVICE_NAME + " instances=" + updatedInstanceAmount;
            try {
                final String res = Util.execCmd(cmd, "");
                log.info(res);
            } catch (IOException e) {
                log.warning("Error during execution of command: '" + cmd + "'  -- " + e.getMessage());
            }

            deploymentTimeout = Util.DEPLOYMENT_TIMEOUT;
        } else if (deploymentTimeout > 0) {
            log.config("There is an AKKA deployment running, waiting " + deploymentTimeout + " seconds for next scaleUp.");
        }
    }

    @Override
    public void scaleUp() {
        scale(Util.NODE_INCREMENT);
    }

    @Override
    void scaleDown() {
        scale(-Util.NODE_INCREMENT);
    }

}
