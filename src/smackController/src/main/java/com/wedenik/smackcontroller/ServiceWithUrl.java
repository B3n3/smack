package com.wedenik.smackcontroller;

class ServiceWithUrl {
    private final String service;
    private final String bean;
    private final String url;

    ServiceWithUrl(String service, String bean, String url) {
        this.service = service;
        this.bean = bean;
        this.url = url;
    }

    String getService() {
        return service;
    }

    String getBean() {
        return bean;
    }

    String getUrl() {
        return url;
    }
}
