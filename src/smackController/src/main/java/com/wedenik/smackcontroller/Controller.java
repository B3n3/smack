package com.wedenik.smackcontroller;

import java.util.*;
import java.util.logging.Logger;

class Controller {

    private final String hostUrl;
    private final Logger log;
    private Map<ServiceWithUrl, Long> thresholds;
    private List<Service> services;
    private Service akka;
    private Service kafka;
    private Service cassandra;
    private Service sparkDataAnalytics;
    private final boolean userInteraction;
    private int initialSparkCores;
    private int initialSparkRAM;

    Controller(String hostUrl, boolean userInteraction, int initialSparkCores, int initialSparkRAM) {
        this.hostUrl = hostUrl;
        this.userInteraction = userInteraction;
        this.initialSparkCores = initialSparkCores;
        this.initialSparkRAM = initialSparkRAM;
        this.log = Logger.getLogger(Controller.class.toString());

        setupServices();
        combineMaps();
    }

    private void setupServices() {
        this.akka = new Akka(hostUrl, "sensor-ingestion");
        this.kafka = new Kafka(hostUrl);
        this.cassandra = new Cassandra(hostUrl);
        this.sparkDataAnalytics = new SparkDataAnalytics(hostUrl, initialSparkCores, initialSparkRAM);

        this.services = new ArrayList<>();
        services.add(akka);
        services.add(kafka);
        services.add(cassandra);
        services.add(sparkDataAnalytics);
    }

    private void combineMaps() {
        thresholds = new HashMap<>();
        for (Service service : services) {
            thresholds.putAll(service.getThresholds());
        }
    }


    void doWork() {
        boolean nothingToDo = true;
        for (ServiceWithUrl obj : thresholds.keySet()) {
            double value = getCurrentValueOfService(obj);
            if (value == -1) continue;
            long threshold = thresholds.get(obj);

            String service = obj.getService();
            String bean = obj.getBean();

            if (value >= threshold) {
                log.info("You should consider scaling up " + service + "\n" + "The value of " + bean + " exceeded the threshold of " + threshold + " and is now **" + value + "**\n\n");
                switch (service) {
                    case "akka":
                        if (askUser("akka")) {
                            akka.scaleUp();
                        }
                        break;
                    case "kafka":
                        if (askUser("kafka")) {
                            kafka.scaleUp();
                        }
                        break;
                    case "cassandra":
                        if (askUser("cassandra")) {
                            cassandra.scaleUp();
                        }
                        break;
                    case "spark-data-analytics":
                        if (askUser("spark-data-analytics")) {
                            sparkDataAnalytics.scaleUp();
                        }
                        break;
                }
                nothingToDo = false;
            } else {
                log.info("Nothing to do for " + service + " : " + bean + ". Threshold: " + threshold + " current: " + value);
            }
        }
        if (nothingToDo) {
            log.info("Everything is fine. Nothing to do.\n\n");
        } else {
            log.info("YOU SHOULD DO SOMETHING!\n");
        }
        log.info("\n----------------------------------------------\n\n");
    }

    private double getCurrentValueOfService(ServiceWithUrl obj) {
        String url = obj.getUrl();
        String req = Util.doGETRequest(url);
        double value;
        try {
            value = Double.valueOf(req);
        } catch (NumberFormatException e) {
            log.warning("Could not parse value into double: '" + req + "' coming from " + obj.getService() + " " + obj.getBean());
            return -1;
        }
        return value;
    }

    /**
     * Asks the user if a service should be scaled
     *
     * @param service The service name to display
     * @return True indicates YES (scale) , false indicates NO (do not scale)
     */
    private boolean askUser(String service) {
        if (!userInteraction) return true;

        Scanner sc = new Scanner(System.in);
        System.out.println("Would you like to scale " + service + "? [y/n]");
        return sc.hasNext() && sc.next().equals("y");
    }

    int numberOfThresholds() {
        int size = 0;
        for (Service s : services) {
            size += s.getThresholds().size();
        }
        return size;
    }


    void decreaseDeploymentTimers() {
        for (Service s : services) {
            s.decreaseDeploymentTimers();
        }
    }
}
