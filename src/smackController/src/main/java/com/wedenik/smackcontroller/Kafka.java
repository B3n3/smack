package com.wedenik.smackcontroller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.logging.Logger;

class Kafka extends Service {

    Kafka(String hostUrl) {
        super(hostUrl);

        log = Logger.getLogger(Kafka.class.toString());
        try {
            initHashes();
        } catch (UnsupportedEncodingException e) {
            log.warning("Error during initialization: " + e.getMessage());
        }
    }

    private void initHashes() throws UnsupportedEncodingException {
        thresholds = new HashMap<>();
        thresholds.put(Util.createServiceWithUrl("kafka", "kafka.network:type=RequestMetrics,name=TotalTimeMs,request=FetchFollower", "Mean", "MAX_60_SEC", hostUrl), 550L);
        thresholds.put(Util.createServiceWithUrl("kafka", "kafka.server:type=ReplicaManager,name=UnderReplicatedPartitions", "Value", "MAX_60_SEC", hostUrl), 1L);
        thresholds.put(Util.createServiceWithUrl("kafka", "kafka.controller:type=KafkaController,name=OfflinePartitionsCount", "Value", "MAX_60_SEC", hostUrl), 1L);
    }

    @Override
    public void scaleUp() {
        if (deploymentTimeout == 0) {
            log.info("I'll go and add another KAFKA Broker for you!");
            scaleUpMarathonEnv("kafka", "BROKER_COUNT");
            deploymentTimeout = Util.DEPLOYMENT_TIMEOUT;
        } else if (deploymentTimeout > 0) {
            log.config("There is a KAFKA deployment running, waiting " + deploymentTimeout + " seconds for next scaleUp.");
        }
    }

    @Override
    void scaleDown() {
        throw new UnsupportedOperationException("Kafka cannot be scaled down in this setup.");
    }

}


