package com.wedenik.smackcontroller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.logging.Logger;

public class SparkDataAnalytics extends Service {
    private static final String HTML_BR = "<br />";
    private final PatternAttributeValue[] thresholdPatterns = {
            new PatternAttributeValue("driver.DAGScheduler.messageProcessingTime", "OneMinuteRate", "MAX_60_SEC", 55),
            new PatternAttributeValue("driver.DAGScheduler.stage.waitingStages", "Value", "MAX_60_SEC", 2),
    };

    private static final String restInterfaceAvailableKeys = "/spark-data-analytics/availableKeys";

    private int cores;
    private int ram;

    SparkDataAnalytics(String hostUrl, int initialSparkCores, int initialSparkRAM) {
        super(hostUrl);
        this.cores = initialSparkCores;
        this.ram = initialSparkRAM;

        log = Logger.getLogger(Kafka.class.toString());
        try {
            initHashes();
        } catch (UnsupportedEncodingException e) {
            log.warning("Error during initialization: " + e.getMessage());
        }
    }

    private void initHashes() throws UnsupportedEncodingException {
        thresholds = new HashMap<>();

        final String availableBeans = Util.doGETRequest(hostUrl + restInterfaceAvailableKeys);
        for (String bean : availableBeans.split(HTML_BR)) {
            for (PatternAttributeValue relevant : thresholdPatterns) {
                if (bean.contains(relevant.pattern)) {
                    bean = bean.replace(HTML_BR, "").trim();
                    thresholds.put(Util.createServiceWithUrl("spark-data-analytics", bean, relevant.attribute, relevant.value, hostUrl), relevant.threshold);
                }
            }
        }
    }

    @Override
    public void scaleUp() {
        increaseRamAndCores();
        scale();
    }

    @Override
    void scaleDown() {
        decreaseRamAndCores();
        scale();
    }

    private void scale() {
        if (deploymentTimeout == 0) {
            log.info("Please go ahead and restart the current SparkDataAnalytics job with those new configuration values\nRAM: " + ram + ", CPU cores: " + cores);
            deploymentTimeout = Util.DEPLOYMENT_TIMEOUT;
        } else if (deploymentTimeout > 0) {
            log.config("There is a SparkDataAnalytics deployment running, waiting " + deploymentTimeout + " seconds for next scaleUp.");
        }
    }

    private void increaseRamAndCores() {
        ram *= Util.SPARK_RAM_INCREMENT;
        cores += Util.SPARK_CORES_INCREMENT;
    }

    private void decreaseRamAndCores() {
        ram /= Util.SPARK_RAM_INCREMENT;
        cores -= Util.SPARK_CORES_INCREMENT;
    }

    private class PatternAttributeValue {
        String pattern;
        String attribute;
        String value;
        long threshold;

        PatternAttributeValue(String pattern, String attribute, String value, long threshold) {
            this.pattern = pattern;
            this.attribute = attribute;
            this.value = value;
            this.threshold = threshold;
        }
    }
}
