package com.wedenik.smackcontroller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

abstract class Service {
    int deploymentTimeout = 0;
    Map<ServiceWithUrl, Long> thresholds;
    Logger log;
    final String hostUrl;

    Service(String hostUrl) {
        this.hostUrl = hostUrl;
    }


    void scaleUpMarathonEnv(String service, String fieldToUpdate) {
        String cmdShowConfig = getMarathonAppConfig(service);
        if (cmdShowConfig == null) return;
        String json = extractEnvFromJSON(cmdShowConfig);

        int fieldValue = getUpdatedFieldValue(service, fieldToUpdate, json, Util.NODE_INCREMENT);
        if (fieldValue == -1) return;
        String jsonUpdated = json.replaceAll("(" + fieldToUpdate + ".*)([0-9]+)", "$1" + fieldValue);

        // pass the updated json via STDIN
        final String updateCmd = "dcos marathon app update " + service;
        try {
            String res = Util.execCmd(updateCmd, jsonUpdated);
            log.info(res);
        } catch (IOException e) {
            log.warning("Could not execute command: '" + updateCmd + "' because of this error: " + e.getMessage());
        }
    }

    private String extractEnvFromJSON(String json) {
        boolean firstLine = true;
        boolean inEnv = true;
        List<String> output = new ArrayList<>();

        // go through all the lines of the received JSON
        // keep the first line
        // keep all lines which are in the "env" list
        for (String line : json.split("\n")) {
            if (firstLine) {
                firstLine = false;
                output.add(line);
            } else {
                // check if we are in env now
                if (line.contains("env")) {
                    inEnv = true;
                }
                // only keep lines if we are in env
                if (inEnv) {
                    // check if we are at the last line of env
                    if (line.contains("},")) {
                        inEnv = false;
                        // replace the "," as we only have one element in the JSON object
                        line = line.replace(",", "");
                    }
                    output.add(line);
                }
            }
        }

        // add the closing curly braces
        output.add("}\n");

        // create a single string
        return output.stream().collect(Collectors.joining("\n"));
    }

    int getUpdatedFieldValue(String service, String fieldToUpdate, String json, int scaleUpIncrement) {
        // increase the broker / node count by scaleUpIncrement
        int fieldValue;
        Pattern pattern = Pattern.compile(fieldToUpdate + ".*([0-9]+)");
        Matcher matcher = pattern.matcher(json);
        if (matcher.find()) {
            fieldValue = Integer.valueOf(matcher.group(1));
        } else {
            log.warning("Could not match broker / node count! No update to " + service.toUpperCase());
            return -1;
        }
        fieldValue += scaleUpIncrement;
        return fieldValue;
    }

    String getMarathonAppConfig(String service) {
        String jsonConfig;
        final String cmd = "dcos marathon app show " + service;
        try {
            jsonConfig = Util.execCmd(cmd, "");
        } catch (IOException e) {
            log.warning("Error during execution of command: '" + cmd + "'  -- " + e.getMessage());
            return null;
        }
        return jsonConfig;
    }

    final void decreaseDeploymentTimers() {
        if (deploymentTimeout > 0) {
            deploymentTimeout -= Util.SLEEP_TIMEOUT;
            if (deploymentTimeout < 0) {
                deploymentTimeout = 0;
            }
        }
    }

    final Map<ServiceWithUrl, Long> getThresholds() {
        return thresholds;
    }

    abstract void scaleUp();

    abstract void scaleDown();
}
