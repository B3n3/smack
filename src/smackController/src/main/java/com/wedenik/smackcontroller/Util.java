package com.wedenik.smackcontroller;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Logger;
import java.util.stream.Collectors;

class Util {
    private static final Logger log = Logger.getLogger(Util.class.toString());
    static final int SLEEP_TIMEOUT = 10;
    static final int DEPLOYMENT_TIMEOUT = 300;

    static final int NODE_INCREMENT = 1;
    static final int SPARK_CORES_INCREMENT = 1;
    static final int SPARK_RAM_INCREMENT = 4;

    static String execCmd(String cmd, String input) throws java.io.IOException {
        final Process process = Runtime.getRuntime().exec(cmd);
        Writer writer = new OutputStreamWriter(process.getOutputStream());
        writer.append(input);
        writer.close();
        java.util.Scanner s = new java.util.Scanner(process.getInputStream()).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    static ServiceWithUrl createServiceWithUrl(String service, String bean, String attribute, String value, String hostUrl) throws UnsupportedEncodingException {
        String url = makeUrl(service, bean, attribute, value, hostUrl);
        return new ServiceWithUrl(service, bean, url);
    }

    private static String makeUrl(String service, String bean, String attribute, String value, String hostUrl) throws UnsupportedEncodingException {
        return hostUrl + "/" + service + "/get/" + URLEncoder.encode(bean, "UTF-8") + "/" + attribute + "/" + value;
    }

    static String doGETRequest(String url) {
        try {
            InputStream inStream = new URL(url).openConnection().getInputStream();
            return new BufferedReader(new InputStreamReader(inStream)).lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            log.warning("Could not open URL " + url + "\n" + e.getMessage());
            return "";
        }
    }
}
