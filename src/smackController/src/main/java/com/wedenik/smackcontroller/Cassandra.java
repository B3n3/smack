package com.wedenik.smackcontroller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.logging.Logger;

class Cassandra extends Service {

    Cassandra(String hostUrl) {
        super(hostUrl);
        log = Logger.getLogger(Cassandra.class.toString());
        initHashes();
    }

    private void initHashes() {
        thresholds = new HashMap<>();
    }

    @Override
    public void scaleUp() {
        if (deploymentTimeout == 0) {
            log.info("I'll go and add another CASSANDRA node for you!");
            scaleUpMarathonEnv("cassandra", "NODES");
            deploymentTimeout = Util.DEPLOYMENT_TIMEOUT;
        } else if (deploymentTimeout > 0) {
            log.config("There is a CASSANDRA deployment running, waiting " + deploymentTimeout + " seconds for next scaleUp.");
        }
    }

    @Override
    void scaleDown() {
        throw new UnsupportedOperationException("Cassandra cannot be scaled down in this setup.");
    }

}


