package com.wedenik.smackcontroller;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Logger log = Logger.getLogger(Main.class.toString());
        Properties props = new Properties();
        String hostUrl;
        boolean userInteraction;
        int initialSparkRAM;
        int initialSparkCores;

        try {
            props.load(Main.class.getResourceAsStream("/application.properties"));
            hostUrl = props.getProperty("restUrl");
            assert (hostUrl != null);

            userInteraction = Boolean.valueOf(props.getProperty("userInteraction"));
            initialSparkRAM = Integer.valueOf(props.getProperty("initialSparkRAM"));
            initialSparkCores = Integer.valueOf(props.getProperty("initialSparkCores"));

        } catch (IOException e) {
            log.severe("Could not read config file: " + e.getMessage());
            return;
        }


        if (args.length > 0) {
            System.out.println("Usage: smackController");
            System.out.println("Please use the properties file instead of the commandline!");
            log.warning("Wrong usage");
            return;
        }

        Controller controller = new Controller(hostUrl, userInteraction, initialSparkCores, initialSparkRAM);

        log.info("Starting to monitor " + controller.numberOfThresholds() + " values");

        while (true) {
            controller.doWork();
            try {
                TimeUnit.SECONDS.sleep(Util.SLEEP_TIMEOUT);
                controller.decreaseDeploymentTimers();
            } catch (InterruptedException e) {
                log.warning("Error during sleep: " + e.getMessage());
            }
        }
    }

}
